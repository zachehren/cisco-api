# CISCO API

*Back-end portion of cisco-front-end (https://bitbucket.org/zachehren/cisco-front-end/src/master/)*

### Configuration

Create database in MySQL:

```
CREATE DATABASE 'all_domains' ;
```

Install the project gems by running:

```
$ bundle install
```

Build project table:

```
$ rake db:migrate
```

### Run the Application

Run the rails server:

```
$ rails s
```
