class AddValidDomainToTable < ActiveRecord::Migration[5.2]
  def change
    add_column :domains, :valid_domain, :boolean, :default => true
  end
end
