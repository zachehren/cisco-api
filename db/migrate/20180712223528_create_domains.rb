class CreateDomains < ActiveRecord::Migration[5.2]
  def change
    create_table :domains do |t|
      t.string :domain_name
      t.string :domain_description
      t.datetime :submitted

      t.timestamps
    end
    add_index :domains, :domain_name, unique: true
  end
end
