class RemoveTimestampsFromDomain < ActiveRecord::Migration[5.2]
  def change
    remove_column :domains, :created_at, :string
    remove_column :domains, :updated_at, :string
  end
end
