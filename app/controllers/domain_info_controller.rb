require 'socket'
class DomainInfoController < ApplicationController

  def index
    valid_domains = Domain.where(valid_domain: true)
    render json: valid_domains
  end

  def create
    begin
      Socket.gethostbyname(params[:domain_name])
    rescue SocketError
      puts "not a valid domain"
      domain = Domain.create!(domain_name: params[:domain_name], domain_description: params[:domain_description], submitted: Time.now, valid_domain: false)
      render json: domain
      return domain
    end
      domain = Domain.create!(domain_name: params[:domain_name], domain_description: params[:domain_description], submitted: Time.now)
      render json: domain
   end

 end
